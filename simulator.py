import logging
import argparse
import json
import sys

from web3 import Web3

from classes.GroupsManager import GroupsManager
from classes.MarketsManager import MarketsManager
from classes.MarketsDefiner import MarketsDefiner
from classes.DataManager import DataManager
from classes.DevicesList import DevicesList
from classes.NGT import NGT
from classes.Utils import Utils

# Main
if __name__ == "__main__":
    # get input arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-o', help='operation mode')
    arg_parser.add_argument('-c', help='configuration file')
    arg_parser.add_argument('-l', help='log file')
    args = arg_parser.parse_args()

    # set operation mode
    operation_mode = args.o
    # print the help
    if operation_mode is None or operation_mode == "HELP":
        Utils.print_help()
        sys.exit()

    # set configuration dictionary
    cfg = json.loads(open(args.c).read())

    # update the configuration with the connection settings
    conns_cfg_file = cfg['conns']['cfgFile']
    cfg['conns'] = json.loads(open(conns_cfg_file).read())
    cfg['conns']['cfgFile'] = conns_cfg_file

    # set logging object
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    # logger
    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=log_file)

    logging.info('Starting program')

    # set the web3 provider instance
    if cfg['web3Provider']['type'] == 'ipc':
        web3 = Web3(Web3.IPCProvider(cfg['web3Provider']['url']))
    elif cfg['web3Provider']['type'] == 'http':
        web3 = Web3(Web3.HTTPProvider(cfg['web3Provider']['url']))

    # define the wallets of the actors with the proper format
    wallets = {
                'owner': web3.toChecksumAddress(web3.eth.accounts[cfg['walletsAccount']['owner']]),
                'dso': web3.toChecksumAddress(web3.eth.accounts[cfg['walletsAccount']['dso']]),
                'player': web3.toChecksumAddress(web3.eth.accounts[cfg['walletsAccount']['player']]),
                'referee': web3.toChecksumAddress(web3.eth.accounts[cfg['walletsAccount']['referee']])
              }

    # define the contracts
    sc_folder = cfg['smartContracts']['truffleProjectFolder']
    contracts = {
                    'ngt': NGT(web3=web3, address=cfg['smartContracts']['NGT']['address'], logger=logger,
                               truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['NGT']['fileName']),
                               gas_price_factor=cfg['gasPriceFactor']),

                    'gsm': GroupsManager(web3=web3, address=cfg['smartContracts']['GroupsManager']['address'],
                                         logger=logger,
                                         truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['GroupsManager']['fileName']),
                                         gas_price_factor=cfg['gasPriceFactor']),

                    'dlist': DevicesList(web3=web3, address=cfg['smartContracts']['DevicesList']['address'],
                                         logger=logger,
                                         truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['DevicesList']['fileName']),
                                         gas_price_factor=cfg['gasPriceFactor']),
                }
    logger.info('Operation mode -> %s' % operation_mode)

    # define the markets manager
    addr_markets_manager = contracts['gsm'].get_address(dso=wallets['dso'])
    markets_manager = MarketsManager(web3=web3, address=addr_markets_manager, logger=logger,
                                     truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['MarketsManager']['fileName']),
                                     gas_price_factor=cfg['gasPriceFactor'])

    # open/settlement operations
    if operation_mode == 'OPEN_NEXT' or operation_mode == 'SETTLE_LAST':

        # define the data manager instance
        data_manager = DataManager(conns_params=cfg['conns'], markets_settings=cfg['marketsSettings'], logger=logger)

        # define the instance to define the markets features
        markets_definer = MarketsDefiner(data_manager=data_manager,
                                         defaults=cfg['smartContracts']['MarketsManager']['defaults'], logger=logger)

        # get the VTN identifier associated to the player
        vtn_id = contracts['dlist'].get_id(device=wallets['player']).decode('utf-8').strip('\x00')
        if len(vtn_id) > 0:
            vtn_id = int(vtn_id)

            if operation_mode == 'OPEN_NEXT':
                market_pars = markets_definer.define_market(vtn_id=vtn_id)
                if market_pars is not False:
                    market_pars['vtn_id'] = vtn_id

                    # Try to open a new market
                    Utils.opening_transactions(markets_manager=markets_manager, wallets=wallets,
                                               market_pars=market_pars, logger=logger)

            elif operation_mode == 'SETTLE_LAST':
                # Try to settle an open market
                market_pars = {'vtn_id': vtn_id, 'type': cfg['smartContracts']['MarketsManager']['defaults']['type']}
                markets_definer.start_dt = Utils.get_start_dt(market_type=market_pars['type'])

                Utils.settling_transactions(markets_manager=markets_manager, markets_definer=markets_definer,
                                            data_manager=data_manager, wallets=wallets,
                                            market_pars=market_pars, logger=logger)
            else:
                logger.warning('Unable to define a market for VTN \'%s\'' % vtn_id)
        else:
            logger.warning('Unable to associate a VTN id to player %s' % wallets['player'])

    # other operations (e.g. PREPARE, MINT, etc.)
    else:
        Utils.auxiliary_commands(web3=web3, operation_mode=operation_mode, wallets=wallets, contracts=contracts,
                                 cfg=cfg, markets_manager=markets_manager, logger=logger)

    logging.info('Ending program')
