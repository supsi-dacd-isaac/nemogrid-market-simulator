import logging
import argparse
import json

from classes.DataManager import DataManager

# Main
if __name__ == "__main__":
    # get input arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', help='configuration file')
    arg_parser.add_argument('-s', help='seconds to wait between two acquisitions')
    arg_parser.add_argument('-l', help='log file')
    args = arg_parser.parse_args()

    # set configuration dictionary
    cfg = json.loads(open(args.c).read())

    # set logging object
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    # set logging object
    if not args.s:
        secs_step = 5
    else:
        secs_step = int(args.s)

    # logger
    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=log_file)

    logging.info('Starting data acquisition')

    data_manager = DataManager(conns_params=cfg, markets_settings=None, logger=logger)

    # Start to acquire data continuously
    data_manager.acquire_data(secs_step=secs_step)

    logging.info('Stopping data acquisition')
