
# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #

import argparse
import json
import logging
from web3 import Web3

from classes.GroupsManager import GroupsManager
from classes.DevicesList import DevicesList
from classes.NGT import NGT
from classes.DataSupplierServer import DataSupplierServer

# Main
if __name__ == "__main__":
    # get input arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-c', help='configuration file')
    arg_parser.add_argument('-l', help='log file')
    args = arg_parser.parse_args()

    # set configuration dictionary
    cfg = json.loads(open(args.c).read())

    # update the configuration with the connection settings
    keys_cfg_file = cfg['server']['enabledKeysFile']
    keys_dict = json.loads(open(keys_cfg_file).read())
    cfg['server']['enabledKeys'] = keys_dict['keys']

    # set logging object
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    # logger
    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=args.l)

    # set the web3 provider instance
    web3 = Web3(Web3.HTTPProvider(cfg['web3Provider']['url']))

    # define the contracts
    sc_folder = cfg['smartContracts']['truffleProjectFolder']
    contracts = {
                    'ngt': NGT(web3=web3, address=cfg['smartContracts']['NGT']['address'], logger=logger,
                               truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['NGT']['fileName']),
                               gas_price_factor=cfg['gasPriceFactor']),

                    'gsm': GroupsManager(web3=web3, address=cfg['smartContracts']['GroupsManager']['address'],
                                         logger=logger,
                                         truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['GroupsManager']['fileName']),
                                         gas_price_factor=cfg['gasPriceFactor']),

                    'dlist': DevicesList(web3=web3, address=cfg['smartContracts']['DevicesList']['address'],
                                         logger=logger,
                                         truffle_output_file='%s/%s' % (sc_folder, cfg['smartContracts']['DevicesList']['fileName']),
                                         gas_price_factor=cfg['gasPriceFactor']),
                }

    # launch the server
    DataSupplierServer(cfg=cfg, contracts=contracts, web3=web3, logger=logger)


