import calendar

from classes.SmartContractInterface import SmartContractInterface as SMI
from classes.Utils import Utils

class DevicesList (SMI):
    """
    Interface class to interact with DevicesList contract
    """

    def __init__(self, web3, address, truffle_output_file, gas_price_factor, logger):
        """
        Constructor

        :param web3: Web3 provider
        :type web3: Web3 provider instance
        :param address: address of the contract
        :type address: string
        :param truffle_output_file: path of the Truffle output file containing the ABI data
        :type truffle_output_file: string
        :param gas_price_factor: factor to apply to gas price
        :type gas_price_factor: float
        :param logger: Logger
        :type logger: logger object
        """
        # set the main parameters
        super().__init__(web3, address, truffle_output_file, gas_price_factor, logger)

    def enable_dso(self, owner, dso):
        """
        Enable a dso to add/remove a device from the list

        :param owner: address of the owner
        :type owner: string
        :param dso: address of the DSO
        :type dso: string
        """

        try:
            tx_pars = {
                        'from': owner,
                        'gas': self.contract.functions.enableDSO(dso).estimateGas({'from': owner}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.enableDSO(dso).transact(tx_pars)
            self._wait_transaction(tx_hash)

            self.logger.info('Enabled DSO %s' % dso)

        except Exception as e:
            return self._exception_handling(e)

    def disable_dso(self, owner, dso):
        """
        Disable a dso to add/remove a device from the list

        :param owner: address of the owner
        :type owner: string
        :param dso: address of the DSO
        :type dso: string
        """

        try:
            tx_pars = {
                        'from': owner,
                        'gas': self.contract.functions.disableDSO(dso).estimateGas({'from': owner}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.disableDSO(dso).transact(tx_pars)
            self._wait_transaction(tx_hash)

            self.logger.info('Disabled DSO %s' % dso)

        except Exception as e:
            return self._exception_handling(e)

    def add(self, adder, device_address, device_id, dso):
        """
        Add a device to the list

        :param adder: address of the adder (owner | enabled DSO)
        :type adder: string
        :param device_address: address of the device (player)
        :type device_address: string
        :param device_id: identifier of the device (player)
        :type device_id: string
        :param dso: address of the DSO
        :type dso: string
        """

        try:
            tx_pars = {
                        'from': adder,
                        'gas': self.contract.functions.add(device_address, device_id, dso).estimateGas({'from': adder}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.add(device_address, device_id, dso).transact(tx_pars)
            self._wait_transaction(tx_hash)

            # transform the string with ASCII hexadecimal codes in the correspondent string of characters
            device_id_str = (bytes.fromhex(device_id)).decode('utf-8')
            self.logger.info('Added in the list the device with id \'%s\' related to player %s' % (device_id_str,
                                                                                                   device_address))
        except Exception as e:
            return self._exception_handling(e)

    def remove(self, remover, device):
        """
        Add a device to the list

        :param remover: address of the remover (owner | enabled DSO)
        :type remover: string
        :param device: address of the device (player)
        :type device: string
        """
        # get the device identifier and properly format it
        device_id = self.get_id(device=device).decode('utf-8').strip('\x00')

        try:
            tx_pars = {
                        'from': remover,
                        'gas': self.contract.functions.remove(device).estimateGas({'from': remover}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.remove(device).transact(tx_pars)
            self._wait_transaction(tx_hash)

            self.logger.info('Removed from the list the device with id \'%s\' related to player %s' % (device_id,
                                                                                                       device))
        except Exception as e:
            return self._exception_handling(e)

    def get_dso_enabling(self, dso):
        return self.contract.functions.getDSOEnabling(dso).call()

    def get_device_flag(self, device):
        return self.contract.functions.getDeviceFlag(device).call()

    def get_id(self, device):
        return self.contract.functions.getId(device).call()

    def get_dso(self, device):
        return self.contract.functions.getDSO(device).call()


