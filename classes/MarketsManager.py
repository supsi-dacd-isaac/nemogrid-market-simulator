import calendar

from classes.SmartContractInterface import SmartContractInterface as SMI
from classes.Utils import Utils

class MarketsManager (SMI):
    """
    Interface class to interact with MarketsManager contract
    """

    def __init__(self, web3, address, truffle_output_file, gas_price_factor, logger):
        """
        Constructor

        :param web3: Web3 provider
        :type web3: Web3 provider instance
        :param address: address of the contract
        :type address: string
        :param truffle_output_file: path of the Truffle output file containing the ABI data
        :type truffle_output_file: string
        :param gas_price_factor: factor to apply to gas price
        :type gas_price_factor: float
        :param logger: Logger
        :type logger: logger object
        """
        # set the main parameters
        super().__init__(web3, address, truffle_output_file, gas_price_factor, logger)

    def open(self, dso, player, referee, pars):
        """
        Open a market

        :param dso: address of the DSO
        :type dso: string
        :param player: address of the player
        :type player: string
        :param referee: address of the referee
        :type referee: string
        :param pars: market parameters
        :type pars: dict
        :return market identifier
        :rtype int
        """

        # set the starting timestamp
        start_dt = Utils.get_start_dt(market_type=pars['type'])
        if start_dt is False:
            return False

        start_ts = calendar.timegm(start_dt.timetuple())

        try:
            tx_pars = {
                        'from': dso,
                        'gas': self.contract.functions.open(player,
                                                            start_ts,
                                                            pars['type'],
                                                            referee,
                                                            pars['maxLower'],
                                                            pars['maxUpper'],
                                                            pars['revenueFactor'],
                                                            pars['penaltyFactor'],
                                                            pars['dsoStaking'],
                                                            pars['playerStaking'],
                                                            pars['percReferee']).estimateGas({'from': dso}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.open(player,
                                                   start_ts,
                                                   pars['type'],
                                                   referee,
                                                   pars['maxLower'],
                                                   pars['maxUpper'],
                                                   pars['revenueFactor'],
                                                   pars['penaltyFactor'],
                                                   pars['dsoStaking'],
                                                   pars['playerStaking'],
                                                   pars['percReferee']).transact(tx_pars)
            self._wait_transaction(tx_hash)

            idx = self.calc_idx(address=player, ts=start_ts, market_type=pars['type'])

            self.logger.info('Created market with idx %s, ts=%i' % (idx, start_ts))
            self.logger.info('Player: %s' % self.get_player(idx))
            self.logger.info('Start time: %i' % self.get_start_time(idx))
            self.logger.info('End time: %i' % self.get_end_time(idx))
            self.logger.info('State: %s' % self.get_state(idx))
            return idx

        except Exception as e:
            self._exception_handling(e)

    def confirm_opening(self, player, idx, staking):
        """
        Confirm a market opening

        :param player: address of the player
        :type player: string
        :param idx: market identifier
        :type idx: int
        :param staking: player staking
        :type staking: int
        """

        try:
            tx_pars = {
                        'from': player,
                        'gas': self.contract.functions.confirmOpening(idx, staking).estimateGas({'from': player}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.confirmOpening(idx, staking).transact(tx_pars)
            self._wait_transaction(tx_hash)

            self.logger.info('Confirmed opening of market with idx %s' % hex(idx))

        except Exception as e:
            self._exception_handling(e)

    def settle(self, dso, idx, power_peak):
        """
        Settle the market

        :param dso: address of the dso
        :type dso: string
        :param idx: market identifier
        :type idx: int
        :param power_peak: measured power peak
        :type power_peak: int
        """
        try:
            tx_pars = {
                        'from': dso,
                        'gas': self.contract.functions.settle(idx, power_peak).estimateGas({'from': dso}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.settle(idx, power_peak).transact(tx_pars)
            self._wait_transaction(tx_hash)

            self.logger.info('Settlement market with idx %s' % hex(idx))

        except Exception as e:
            self._exception_handling(e)

    def confirm_settlement(self, player, idx, power_peak):
        """
        Confirm a settlement

        :param player: address of the player
        :type player: string
        :param idx: market identifier
        :type idx: int
        :param power_peak: measured power peak
        :type power_peak: int
        """
        try:
            tx_pars = {
                        'from': player,
                        'gas': self.contract.functions.confirmSettlement(idx, power_peak).estimateGas({'from': player}),
                        'gasPrice': int(self.web3.eth.gasPrice*self.gas_price_factor)
                      }

            tx_hash = self.contract.functions.confirmSettlement(idx, power_peak).transact(tx_pars)
            self._wait_transaction(tx_hash)

            self.logger.info('Settlement market with idx %s' % hex(idx))

        except Exception as e:
            self._exception_handling(e)

    def get_state(self, idx):
        return int(self.contract.functions.getState(idx).call())

    def get_flag(self, idx):
        return self.contract.functions.getFlag(idx).call()

    def get_start_time(self, idx):
        return self.contract.functions.getStartTime(idx).call()

    def get_end_time(self, idx):
        return self.contract.functions.getEndTime(idx).call()

    def get_player(self, idx):
        return self.contract.functions.getPlayer(idx).call()

    def calc_idx(self, address, ts, market_type):
        return self.contract.functions.calcIdx(address, ts, market_type).call()

    def get_dso_stake(self, idx):
        return self.contract.functions.getDsoStake(idx).call()

    def get_player_stake(self, idx):
        return self.contract.functions.getPlayerStake(idx).call()

    def get_tokens_released_to_dso(self, idx):
        return self.contract.functions.getTknsReleasedToDSO(idx).call()

    def get_tokens_released_to_player(self, idx):
        return self.contract.functions.getTknsReleasedToPlayer(idx).call()

    def get_upper_maximum(self, idx):
        return self.contract.functions.getUpperMaximum(idx).call()

    def get_lower_maximum(self, idx):
        return self.contract.functions.getLowerMaximum(idx).call()

    def get_dso_maximum(self, idx):
        return self.contract.functions.getPowerPeakDeclaredByDSO(idx).call()

    def get_player_maximum(self, idx):
        return self.contract.functions.getPowerPeakDeclaredByPlayer(idx).call()

