import random
import random
import time
import json
import sys

from influxdb import InfluxDBClient
from datetime import datetime, timedelta

import Constants

from classes.Utils import Utils

class DataManager:
    """
    Interface class able to retrieve measurement data from a remote API and save all in an InfluxDB database
    """

    def __init__(self, conns_params, markets_settings, logger):
        """
        Constructor

        :param conns_params: parameters needed to retrieve and store the measurement data
        :type conns_params: dict
        :param markets_settings: market settings
        :type markets_settings: dict
        :param logger: logger
        :type logger: logger object
        """
        # set the main parameters
        self.conns_params = conns_params
        self.markets_settings = markets_settings
        self.logger = logger

        # set the API URL to get measurements data
        self.remote_api_measurements = 'http://%s:%i%s' % (self.conns_params['webServices']['measurementsData']['host'],
                                                           self.conns_params['webServices']['measurementsData']['port'],
                                                           self.conns_params['webServices']['measurementsData']['service'])

        # set the API URL to get forecasts data
        self.remote_api_forecasts = 'http://%s:%i%s' % (self.conns_params['webServices']['forecastsData']['host'],
                                                        self.conns_params['webServices']['forecastsData']['port'],
                                                        self.conns_params['webServices']['forecastsData']['service'])

        self.logger.info("Connection to InfluxDB server on [%s:%s]" % (self.conns_params['influxDB']['host'],
                                                                       self.conns_params['influxDB']['port']))

        # set the InfluxDB client instance
        try:
            self.idb_client = InfluxDBClient(host=self.conns_params['influxDB']['host'],
                                             port=int(self.conns_params['influxDB']['port']),
                                             username=self.conns_params['influxDB']['user'],
                                             password=self.conns_params['influxDB']['password'],
                                             database=self.conns_params['influxDB']['db'])
        except Exception as e:
            self.logger.error('EXCEPTION: %s' % str(e))
            sys.exit(2)
        self.logger.info('Connection successful')

    def get_random_maximum(self):
        """
        Return a random power maximum

        :return: the power maximum [kW]
        :rtype: float
        """
        return random.randint(self.markets_settings['minPower'], self.markets_settings['maxPower'])

    def get_maximum(self, vtn_id, params):
        """
        Return the power maximum

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :param params: parameters needed to calculate the peak
        :type params: dict
        :return: the power maximum [kW]
        :rtype: int
        """
        maximum_power = None

        # daily markets
        if params['type'] == Constants.DAILY_MARKETS:
            tmp_dt = params['start_dt'] - timedelta(days=2)
            start_dt_query = tmp_dt.replace(hour=0, minute=0)
            end_dt_query = tmp_dt.replace(hour=23, minute=59)

        # hourly markets
        elif params['type'] == Constants.HOURLY_MARKETS:
            # Go back for 24 additional hours to be sure to have some data in the DB
            tmp_dt = params['start_dt'] - timedelta(hours=1+24)
            start_dt_query = tmp_dt.replace(minute=0)
            end_dt_query = tmp_dt.replace(minute=59)

        str_start = start_dt_query.strftime('%Y-%m-%dT%H:%M:%SZ')
        str_end = end_dt_query.strftime('%Y-%m-%dT%H:%M:%SZ')

        query = 'SELECT MAX(value) FROM %s WHERE substation_id=\'%i\' AND ' \
                'time>=\'%s\' AND time<=\'%s\'' % (self.conns_params['influxDB']['measurementData'],
                                                   vtn_id, str_start, str_end)
        self.logger.info(query)

        # perform the InfluxDB query
        try:
            res = self.idb_client.query(query=query)
            maximum_power = int(round(res.raw['series'][0]['values'][0][1], 0))
        except Exception as e:
            self.logger.error('Exception: %s' % str(e))

        # return the maximum power
        return maximum_power

    def influxdb_forecast(self, vtn_id, params):
        """
        Return the maximum thresholds using a forecast saved in an InfluxDB server

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :param params: parameters needed to calculate the peak
        :type params: dict
        :return: the maximum thresholds [kW], which can be used in the markets or None if an exception occurs
        :rtype: dict | None
        """
        return self.persistence(vtn_id=vtn_id, params=params)

    def remote_forecast(self, vtn_id, params):
        """
        Return the maximum thresholds using a remote forecaster

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :param params: parameters needed to calculate the peak
        :type params: dict
        :return: the maximum thresholds [kW], which can be used in the markets or None if an exception occurs
        :rtype: dict | None
        """
        power_thresholds = None

        # Get measurement data
        data_for_forecast = self._retrieve_values_for_forecast(vtn_id=vtn_id, params=params)

        # perform the request
        r = Utils.perform_request(req_type=self.conns_params['webServices']['forecastsData']['request'],
                                  req_url=self.remote_api_forecasts, req_params=data_for_forecast, logger=self.logger)

        # check if the request has been successful
        if r is not None and r.status_code == 200:
            # extract forecast value from the request body
            dataset = json.loads(r.text)
            power_thresholds = dict(maxUpper=int(round(dataset[str(vtn_id)] * 0.9, 0)),
                                    maxLower=int(round(dataset[str(vtn_id)] * 0.6, 0)))

            # return the thresholds
            self.logger.info('Remote forecast for the next market = %.2f kW' % dataset[str(vtn_id)])
            self.logger.info('Maximum thresholds: lower = %.1f kW; upper = %.1f kW' % (power_thresholds['maxLower'],
                                                                                       power_thresholds['maxUpper']))
        else:
            self.nok_request_handling(r, self.remote_api_forecasts)

        return power_thresholds

    def _retrieve_values_for_forecast(self, vtn_id, params):
        """
        Return the measurements data that have to be used by the remote forecaster

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :param params: parameters needed to calculate the peak
        :type params: dict
        :return: dictionary containing the measurements data
        :rtype: dict
        """
        # daily markets
        if params['type'] == Constants.DAILY_MARKETS:
            tmp_dt = params['start_dt'] - timedelta(days=7)
            start_dt_query = tmp_dt.replace(hour=0, minute=0)
            end_dt_query = tmp_dt.replace(hour=23, minute=59)

        # hourly markets
        elif params['type'] == Constants.HOURLY_MARKETS:
            tmp_dt = params['start_dt'] - timedelta(days=1)
            start_dt_query = tmp_dt.replace(minute=0)
            end_dt_query = tmp_dt.replace(minute=59)

        str_start = start_dt_query.strftime('%Y-%m-%dT%H:%M:%SZ')
        str_end = end_dt_query.strftime('%Y-%m-%dT%H:%M:%SZ')

        query = 'SELECT MAX(value) FROM %s WHERE VTN=\'VTN_%02d\' AND ' \
                'time>=\'%s\' AND time<=\'%s\' GROUP BY TIME(1h)' % (self.conns_params['influxDB']['measurementData'],
                                                                     vtn_id, str_start, str_end)

        # perform the InfluxDB query
        points = self._get_historical_data(vtn_id=vtn_id, query=query, start_dt_query=start_dt_query,
                                           market_type=params['type'])

        return points

    def _get_historical_data(self, vtn_id, query, start_dt_query, market_type):
        """
        Get historical data from InfluxDB server

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :param query: InfluxQL query
        :type query: string
        :param start_dt_query: starting datetime used in the query
        :type start_dt_query: datetime
        :param market_type: market type (1: daily | 2: hourly)
        :type market_type: int
        :return: the data points
        :rtype: dict
        """
        points = dict(vtn=vtn_id)
        # perform the InfluxDB query

        self.logger.info(query)
        try:
            res = self.idb_client.query(query=query, epoch='s')

            # extract the needed values/timestamps from the dictionary
            if 'series' in res.raw.keys():
                for point in res.raw['series'][0]['values']:

                    if market_type == Constants.DAILY_MARKETS:
                        # check if the the value is meaningful
                        if point[1] is not None:
                            points[point[0]] = float(point[1])

                    elif market_type == Constants.HOURLY_MARKETS:
                        # check if the past hours correspond to new market's one
                        point_dt = datetime.utcfromtimestamp(point[0])
                        if start_dt_query.hour == point_dt.hour and point[1] is not None:
                            points[point[0]] = float(point[1])

        except Exception as e:
            self.logger.error('Exception: %s' % str(e))

        return points

    def persistence(self, vtn_id, params):
        """
        Return the maximum thresholds using persistence model

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :param params: parameters needed to calculate the peak
        :type params: dict
        :return: the maximum thresholds [kW], which can be used in the markets or None if an exception occurs
        :rtype: dict | None
        """
        power_thresholds = None

        # daily markets
        if params['type'] == Constants.DAILY_MARKETS:
            tmp_dt = params['start_dt'] - timedelta(days=7)
            start_dt_query = tmp_dt.replace(hour=0, minute=0)
            end_dt_query = tmp_dt.replace(hour=23, minute=59)

        # hourly markets
        elif params['type'] == Constants.HOURLY_MARKETS:
            # Go back for 24 additional hours to be synced with the forecast (see get_maximum function for details)
            start_dt_query = params['start_dt'] - timedelta(hours=(24*7)+24)
            end_dt_query = params['start_dt'] - timedelta(hours=(24*6)+23+24)

        str_start = start_dt_query.strftime('%Y-%m-%dT%H:%M:%SZ')
        str_end = end_dt_query.strftime('%Y-%m-%dT%H:%M:%SZ')

        query = 'SELECT MAX(value) FROM %s WHERE substation_id=\'%i\' AND ' \
                'time>=\'%s\' AND time<\'%s\'' % (self.conns_params['influxDB']['measurementData'], vtn_id, str_start,
                                                  str_end)
        self.logger.info(query)

        # perform the InfluxDB query
        try:
            res = self.idb_client.query(query=query)
            if 'series' in res.raw.keys():
                power_thresholds = dict(maxUpper=int(round(res.raw['series'][0]['values'][0][1]*0.9, 0)),
                                        maxLower=int(round(res.raw['series'][0]['values'][0][1]*0.6, 0)))
        except Exception as e:
            self.logger.error('Exception: %s' % str(e))

        # return the thresholds
        return power_thresholds

    def acquire_data(self, secs_step):
        """
        Acquire data continuously from the remote API

        :param secs_step: step to wait between two requests (secs)
        :type secs_step: int
        """

        # cycle until the market will be finished
        while True:

            # acquire data from the remote source and save all in the database
            data_points = []

            # cycle over the VTNs
            for vtn in self.conns_params['webServices']['measurementsData']['vtns']:

                # perform the request
                r = Utils.perform_request(req_type=self.conns_params['webServices']['measurementsData']['request'],
                                          req_url=self.remote_api_measurements, req_params={'vtn': int(vtn)},
                                          logger=self.logger)

                # check if the request has been successful
                if r is not None and r.status_code == 200:

                    # extract measurement from the request body
                    dataset = json.loads(r.text)
                    for data in dataset['%i' % vtn]:
                        # build the dictionary point for InfluxDB and append it in the list
                        point = {
                                    'time': int(data['ts']),
                                    'measurement': self.conns_params['influxDB']['measurementData'],
                                    'fields': {'value': float(data['power'])},
                                    'tags': {'VTN': 'VTN_%02i' % vtn}
                                }
                        data_points.append(point)

                    # wait for 100 ms if there are more VTNs
                    if len(self.conns_params['webServices']['measurementsData']['vtns']) > 1:
                        time.sleep(0.1)
                else:
                    self.nok_request_handling(r, self.remote_api_measurements)

            # save the acquired data in InfluxDB
            self.logger.info('Write %i points in InfluxDB server' % len(data_points))
            self.idb_client.write_points(points=data_points, time_precision='s')

            # wait until the next acquisition
            self.logger.info('Wait for %i seconds' % secs_step)
            time.sleep(secs_step)

    def nok_request_handling(self, r, ws):
        """
        Handle a not successful request

        :param r: request object | None
        :type r: request
        :param ws: URL of the web service
        :type ws: string
        """
        if r is not None:
            err_msg = json.loads(r.text)
            self.logger.error('Request not successful, code: %i, desc: \"%s\"' % (r.status_code, err_msg['desc']))
        else:
            self.logger.error('Service %s not available' % ws)

        # wait for 200 ms before going on
        time.sleep(0.2)
