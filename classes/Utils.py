import calendar
import sys
import time
import requests

from calendar import monthrange
from datetime import datetime, timedelta

import Constants

class Utils:
    """
    Class containing static utilities functions
    """

    @staticmethod
    def get_start_dt(market_type, market_case=1):
        """
        Get starting date/time object for a market

        :param market_type: market type (0: monthly, 1: daily, 2: hourly)
        :type market_type: int
        :param market_case: market case (-1: last | 0: current | 1: next)
        :type market_case: int
        :return: starting datetime | False
        :rtype: datetime | bool
        """

        # set the starting timestamp
        utc_now = datetime.utcnow()

        # check the market case value
        if market_case != -1 and market_case != 0 and market_case != 1:
            return False

        start_dt = False

        # daily markets
        if market_type == Constants.DAILY_MARKETS:
            start_dt = utc_now + timedelta(days=1*market_case)
            start_dt = datetime(start_dt.year, start_dt.month, start_dt.day, 0, 0, 0)

        # hourly markets
        elif market_type == Constants.HOURLY_MARKETS:
            start_dt = utc_now + timedelta(hours=1*market_case)
            start_dt = datetime(start_dt.year, start_dt.month, start_dt.day, start_dt.hour, 0, 0)

        return start_dt

    @staticmethod
    def print_market_info(markets_manager, market_type, start_dt, player, logger):
        """
        Print market info

        :param markets_manager: instance handling the energy markets
        :type markets_manager: MarketsManager
        :param market_type: market type (0: monthly | 1: daily | 2: hourly)
        :type market_type: int
        :param start_dt: starting datetime of the market
        :type start_dt: datetime
        :param player: market player
        :type player: string
        :param logger: logger instance
        :type logger: logger
        """

        # get the market id
        idx = markets_manager.calc_idx(player, int(calendar.timegm(start_dt.timetuple())), market_type)

        start_ts = markets_manager.get_start_time(idx=idx)
        end_ts = markets_manager.get_end_time(idx=idx)
        state = markets_manager.get_state(idx=idx)

        # print data
        logger.info('MARKETS:')
        logger.info('INPUT PARAMETERS:')
        logger.info('PLAYER: %s' % player)
        logger.info('START TIME: %s' % start_dt.strftime('%Y-%m-%d %H:%M:%S'))
        logger.info('STATE: %s' % Constants.MARKETS_STATES[state])
        # Check if the state is not NONE
        if state != 0:
            logger.info('DATA:')
            logger.info('Id: %s' % hex(idx))
            logger.info('Start time: %s' % datetime.utcfromtimestamp(start_ts).strftime('%Y-%m-%d %H:%M:%S'))
            logger.info('End time: %s' % datetime.utcfromtimestamp(end_ts).strftime('%Y-%m-%d %H:%M:%S'))
            logger.info('DSO staking: %i NGT' % markets_manager.get_dso_stake(idx=idx))
            logger.info('Player staking: %i NGT' % markets_manager.get_player_stake(idx=idx))
            logger.info('Maximum measured power: %i kW' % markets_manager.get_dso_maximum(idx=idx))
            logger.info('Lower threshold: %i kW' % markets_manager.get_lower_maximum(idx=idx))
            logger.info('Upper threshold: %i kW' % markets_manager.get_upper_maximum(idx=idx))
            logger.info('Maximum measured power: %i kW' % markets_manager.get_dso_maximum(idx=idx))
            logger.info('Tokens released to DSO: %i NGT' % markets_manager.get_tokens_released_to_dso(idx=idx))
            logger.info('Tokens released to Player: %i NGT' % markets_manager.get_tokens_released_to_player(idx=idx))

    @staticmethod
    def opening_transactions(markets_manager, wallets, market_pars, logger):
        """
        Markets opening transactions

        :param markets_manager: instance handling the energy markets
        :type markets_manager: MarketsManager
        :param wallets: wallets playing the market
        :type wallets: dict
        :param contracts: dictionary containing the contracts instances
        :type contracts: dict
        :param market_pars: market parameters
        :type market_pars: dict
        :param cfg: configuration parameters
        :type cfg: dict
        :param logger: logger instance
        :type logger: logger
        """
        # open a new market and confirm it
        start_dt_opening = Utils.get_start_dt(market_type=market_pars['type'])

        # calculate idx of the market to open
        idx_market_to_open = markets_manager.calc_idx(wallets['player'],
                                                      int(calendar.timegm(start_dt_opening.timetuple())),
                                                      market_pars['type'])

        logger.info('Try to open a new market')
        logger.info('Id: %s' % hex(idx_market_to_open))
        logger.info('Start time: %s' % start_dt_opening.strftime('%Y-%m-%d %H:%M:%S'))

        # check if it is already open
        if markets_manager.get_state(idx=idx_market_to_open) == 0:
            # check if a confirmed market can be created
            if market_pars is False:
                logger.warning('Unable to define a confirmed market, exit the program')
                sys.exit(-1)

            # get balances before the settlement

            logger.info('Open the market')
            markets_manager.open(dso=wallets['dso'], player=wallets['player'], referee=wallets['referee'],
                                 pars=market_pars)
            logger.info('Market state[%s]: %i' % (hex(idx_market_to_open),
                                                   markets_manager.get_state(idx=idx_market_to_open)))

            # wait for a second
            time.sleep(1)

            logger.info('Confirm the market opening')
            markets_manager.confirm_opening(player=wallets['player'], idx=idx_market_to_open,
                                            staking=market_pars['playerStaking'])

            # print the market state
            Utils.print_market_info(markets_manager=markets_manager, market_type=market_pars['type'],
                                    start_dt=start_dt_opening, player=wallets['player'], logger=logger)

            # wait for a second
            time.sleep(1)
        else:
            logger.warning('Market %s already open' % (hex(idx_market_to_open)))

            # get the market state
            market_state = Constants.MARKETS_STATES[markets_manager.get_state(idx=idx_market_to_open)]
            logger.info('Market state[%s]: %s' % (hex(idx_market_to_open), market_state))

    @staticmethod
    def settling_transactions(markets_manager, markets_definer, data_manager, wallets, market_pars, logger):
        """
        Markets settling transactions

        :param markets_manager: instance handling the energy markets
        :type markets_manager: MarketsManager
        :param markets_definer: instance handling the energy needed inputs
        :type markets_definer: MarketsDefiner
        :param data_manager: instance handling the data measurements
        :type data_manager: DataManager
        :param wallets: wallets playing the market
        :type wallets: dict
        :param contracts: dictionary containing the contracts instances
        :type contracts: dict
        :param market_pars: market parameters
        :type market_pars: dict
        :param logger: logger instance
        :type logger: logger
        """
        start_dt_settlement = Utils.get_start_dt(market_type=market_pars['type'], market_case=-1)

        # calculate idx of the market to close
        idx_market_to_settle = markets_manager.calc_idx(wallets['player'],
                                                        int(calendar.timegm(start_dt_settlement.timetuple())),
                                                        market_pars['type'])

        logger.info('Try to settle an open market')
        logger.info('Id: %s' % hex(idx_market_to_settle))
        logger.info('Start time: %s' % start_dt_settlement.strftime('%Y-%m-%d %H:%M:%S'))

        # check if it is ready to be closed, i.e. it is in RUNNING (3) mode
        if markets_manager.get_state(idx=idx_market_to_settle) == 3:

            # get maximum power
            power_peak = data_manager.get_maximum(vtn_id=market_pars['vtn_id'],
                                                  params={'type': markets_definer.market_type,
                                                          'start_dt': markets_definer.start_dt})

            # settle the market
            logger.info('Settle the market')
            markets_manager.settle(dso=wallets['dso'], idx=idx_market_to_settle, power_peak=power_peak)

            # get the market state
            market_state = Constants.MARKETS_STATES[markets_manager.get_state(idx=idx_market_to_settle)]
            logger.info('Market state[%s]: %s' % (hex(idx_market_to_settle), market_state))

            # wait for a second
            time.sleep(1)

            # confirm the settlement
            logger.info('Confirm the market settlement')
            markets_manager.confirm_settlement(player=wallets['player'], idx=idx_market_to_settle,
                                               power_peak=power_peak)

            # print the market state
            Utils.print_market_info(markets_manager=markets_manager, market_type=market_pars['type'],
                                    start_dt=start_dt_settlement, player=wallets['player'], logger=logger)

            # wait for a second
            time.sleep(1)
        else:
            logger.warning('Market %s not in RUNNING state, it cannot be settled' % (hex(idx_market_to_settle)))

            # get the market state
            market_state = Constants.MARKETS_STATES[markets_manager.get_state(idx=idx_market_to_settle)]
            logger.info('Market state[%s]: %s' % (hex(idx_market_to_settle), market_state))

    @staticmethod
    def auxiliary_commands(web3, operation_mode, wallets, contracts, cfg, markets_manager, logger):
        """
        Auxiliary commands (e.g. MINT, BALANCE, etc.)

        :param web3: web3 provider
        :type web3: Web3
        :param operation_mode: operation mode containing the command to actuate and (eventually) additional parameters
        :type operation_mode: string
        :param wallets: wallets playing the market
        :type wallets: dict
        :param contracts: dictionary containing the contracts instances
        :type contracts: dict
        :param cfg: configuration parameters
        :type cfg: dict
        :param markets_manager: instance handling the energy markets
        :type markets_manager: MarketsManager
        :param logger: logger instance
        :type logger: logger
        """

        data = operation_mode.split(',')
        cmd = data[0]
        args = data[1:len(data)]
        market_type = cfg['smartContracts']['MarketsManager']['defaults']['type']

        # mint new NGTs
        if cmd == 'MINT':
            address = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            amount = args[1]

            # mint new tokens
            contracts['ngt'].mint(minter=wallets['owner'], beneficiary=address, amount=int(amount))
            logger.info('Minted %s NGTs and assigned to %s, current balance = %s NGT' % (amount, address,
                                                                                         contracts['ngt'].balance(address)))
        # get balance
        elif cmd == 'BALANCE':
            address = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            # Get balance
            logger.info('Amount[%s] = %i NGT' % (address, contracts['ngt'].balance(address)))

        # get balance for a given address
        elif cmd == 'BALANCE_ADDR':
            address = web3.toChecksumAddress(args[0])
            logger.info('Amount[%s] = %i NGT' % (address, contracts['ngt'].balance(address)))

        # get allowance
        elif cmd == 'ALLOWANCE':
            owner = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            spender = web3.toChecksumAddress(args[1])
            logger.info('Allowance[%s->%s] = %i NGT' % (owner, spender, contracts['ngt'].allowance(owner, spender)))

        # get address of the markets manager for a given dso
        elif cmd == 'ADDR_MARKETS_MANAGER':
            dso = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            logger.info('Address = %s' % contracts['gsm'].get_address(dso))

        # modify token allowance
        elif cmd == 'ALLOW':
            owner = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            spender = web3.toChecksumAddress(args[1])
            amount = args[2]

            # increase the token allowance
            contracts['ngt'].increase_allowance(allower=owner, beneficiary=spender, amount=int(amount))
            logger.info('Allowance of %s for %s set to %i NGT' % (owner, spender,
                                                                  contracts['ngt'].allowance(owner, spender)))

        # add a group of markets
        elif cmd == 'ADD_GROUP':
            owner = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            dso = web3.toChecksumAddress(web3.eth.accounts[int(args[1])])

            # increase the token allowance
            contracts['gsm'].add_group(owner=owner, dso=dso)
            logger.info('Group for DSO %s available at address %s' % (dso, contracts['gsm'].get_address(dso)))

        # prepare a couple dso-player to play a set of markets
        elif cmd == 'PREPARE':
            if contracts['gsm'].get_flag(dso=wallets['dso']) is False:
                # create group
                logger.info('Create group for DSO %s' % wallets['dso'])
                contracts['gsm'].add_group(owner=wallets['owner'], dso=wallets['dso'])

            # get address of the market manager
            addr_mm = contracts['gsm'].get_address(dso=wallets['dso'])

            # mint token
            logger.info('Mint tokens')
            contracts['ngt'].mint(minter=wallets['owner'], beneficiary=wallets['dso'],
                                  amount=cfg['marketsSettings']['tokens']['minting']['forDSO'])
            contracts['ngt'].mint(minter=wallets['owner'], beneficiary=wallets['player'],
                                  amount=cfg['marketsSettings']['tokens']['minting']['forPlayer'])
            contracts['ngt'].mint(minter=wallets['owner'], beneficiary=wallets['referee'],
                                  amount=cfg['marketsSettings']['tokens']['minting']['forReferee'])
            # set allowance
            logger.info('Set tokens allowance')
            contracts['ngt'].increase_allowance(allower=wallets['dso'], beneficiary=addr_mm,
                                                amount=cfg['marketsSettings']['tokens']['allowance']['DSO2Market'])
            contracts['ngt'].increase_allowance(allower=wallets['player'], beneficiary=addr_mm,
                                                amount=cfg['marketsSettings']['tokens']['allowance']['Player2Market'])

            # enable the dso update di devices list
            contracts['dlist'].enable_dso(owner=wallets['owner'], dso=wallets['dso'])

            # add device corresponding to player in the devices list
            contracts['dlist'].add(adder=wallets['owner'], device_id=Utils.get_device_id(identifier=Constants.VTN_ID),
                                   device_address=wallets['player'], dso=wallets['dso'])

        # enable a DSO to update the devices list
        elif cmd == 'ENABLE_DSO':
            dso = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            contracts['dlist'].enable_dso(owner=wallets['owner'], dso=dso)

        # disable a DSO to update the devices list
        elif cmd == 'DISABLE_DSO':
            dso = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            contracts['dlist'].disable_dso(owner=wallets['owner'], dso=dso)

        # add a device in the list
        elif cmd == 'ADD_DEVICE':
            device = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            device_id = int(args[1])
            dso = web3.toChecksumAddress(web3.eth.accounts[int(args[2])])
            contracts['dlist'].add(adder=wallets['owner'], device_id=Utils.get_device_id(identifier=device_id),
                                   device_address=device, dso=dso)

        # remove a device from the list
        elif cmd == 'REMOVE_DEVICE':
            device = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            contracts['dlist'].remove(remover=wallets['owner'], device=device)

        # get device id
        elif cmd == 'GET_DEVICE_ID':
            device = web3.toChecksumAddress(web3.eth.accounts[int(args[0])])
            device_id = contracts['dlist'].get_id(device=device)
            logger.info('Player address \'%s\' <-> device id \'%s\'' % (device,
                                                                        device_id.decode('utf-8').strip('\x00')))

        # get data about the state of a specific market
        elif cmd == 'STATE':
            try:
                if args[0] == 'CURR':
                    start_dt = Utils.get_start_dt(market_type=market_type, market_case=0)
                elif args[0] == 'NEXT':
                    start_dt = Utils.get_start_dt(market_type=market_type, market_case=1)
                elif args[0] == 'LAST':
                    start_dt = Utils.get_start_dt(market_type=market_type, market_case=-1)
                else:
                    start_dt = datetime.strptime(args[0], '%Y-%m-%dT%H:%M:%SZ')

                # print the market info
                Utils.print_market_info(markets_manager=markets_manager, market_type=market_type, start_dt=start_dt,
                                        player=wallets['player'], logger=logger)
            except Exception as e:
                logger.error('Exception: %s' % str(e))

        # not available commands
        else:
            logger.warning('Command %s is not available' % operation_mode)

    @staticmethod
    def get_device_id(identifier):
        """
        Get a device id properly formatted

        :param identifier: device identifier
        :type identifier: int
        :return: formatted device identifier
        :rtype: string
        """
        # return the formatted device id (numerical id -> strind -> UTF-8 encoding -> hex format)
        return ('%i' % identifier).encode('utf-8').hex()

    @staticmethod
    def perform_request(req_type, req_url, req_params, logger):
        """
        Perform a HTTP request

        :param req_type: request type
        :type req_type: str
        :param req_url: request URL
        :type req_url: str
        :param req_params: parameters to send in the request
        :type req_url: dict
        :param logger: logger object
        :type logger: logger
        :return: request object
        :rtype: request | None
        """
        try:
            if req_type == 'POST':
                return requests.post(req_url, params=req_params)
            else:
                return requests.get(req_url, params=req_params)
        except Exception as e:
            logger.error('Exception: %s' % str(e))
            return None

    @staticmethod
    def print_help():
        """
        Print the help
        """

        print('')
        print(' *** Available commands (XYZ_ADDRESS=1 => XYZ_ADDRESS=eth.accounts[1]):\n')

        print('ADD_DEVICE: Add a device to the list (i.e. a device is the player identifier in the facility')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o ADD_DEVICE,PLAYER_ADDRESS,PLAYER_ID,DSO_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o ADD_DEVICE,2,1976,1')
        print('')

        print('ADD_GROUP: Add a group')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o ADD_GROUP,OWNER_ADDRESS,DSO_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o ADD_GROUP,0,1')
        print('')

        print('ADDR_MARKETS_MANAGER: Get the address of the markets manager of a DSO')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o ADDR_MARKETS_MANAGER,DSO_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o ADDR_MARKETS_MANAGER,1')
        print('')

        print('ALLOW: Allow an address to get NGTs from another address')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o ALLOW,ALLOWER_ADDRESS,ALLOWED_ACCOUNT,AMOUNT')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o ALLOW,1,0x01,10')
        print('')

        print('ALLOWANCE: Get allowance status')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o ALLOWANCE,ALLOWER_ADDRESS,ALLOWED_ACCOUNT')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o ALLOWANCE,1,0x01')
        print('')

        print('BALANCE: Get NGT balance')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o BALANCE,ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o BALANCE,1')
        print('')

        print('BALANCE_ADDR: Get NGT balance for a specific address')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o BALANCE_ADDR,ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o BALANCE_ADDR,0x01')
        print('')

        print('DISABLE_DSO: Disable a DSO to update the devices list')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o DISABLE_DSO,DSO_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o DISABLE_DSO,1')
        print('')

        print('ENABLE_DSO: Enable a DSO to update the devices list')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o ENABLE_DSO,DSO_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o ENABLE_DSO,1')
        print('')

        print('GET_DEVICE_ID: Get the device identifier of the player')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o GET_DEVICE_ID,PLAYER_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o GET_DEVICE_ID,2')
        print('')

        print('MINT: Mint NGTs token for an account')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o MINT,RECEIVER_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o MINT,2')
        print('')

        print('OPEN_NEXT: Open the next hourly/daily market')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o OPEN_NEXT')
        print('')

        print('PREPARE: Prepare a couple DSO-player to play a set of markets given a configuration file')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o PREPARE')
        print('')

        print('REMOVE_DEVICE: Remove a device from the list (i.e. a device is the player identifier in the facility')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o REMOVE_DEVICE,PLAYER_ADDRESS')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o REMOVE_DEVICE,2')
        print('')

        print('SETTLE_LAST: Settle the last hourly/daily market')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o SETTLE_LAST')
        print('')

        print('STATE: Get the state of a market')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o STATE,NEXT')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o STATE,CURR')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o STATE,LAST')
        print('    Usage:    # venv/bin/python simulator.py -c cfg.json -o STATE,YYYY-mm-ddTHH:MM:SSZ')
        print('    Example:  # venv/bin/python simulator.py -c cfg.json -o STATE,2020-05-12T12:00:00Z')
        print('')
