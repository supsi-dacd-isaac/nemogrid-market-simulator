import time
import copy
import json

from classes.Utils import Utils

class MarketsDefiner:
    """
    Class able to define a market with the proper settings
    """

    def __init__(self, data_manager, defaults, logger):
        """
        Constructor

        :param data_manager: DataManager instance
        :type data_manager: DataManager
        :param defaults: defaults parameters
        :type defaults: dict
        :param logger: Logger
        :type logger: logger object
        """
        # set the main parameters
        self.data_manager = data_manager
        self.defaults = defaults
        self.logger = logger
        self.start_dt = None
        self.market_type = self.defaults['type']

        # set the API URL to get markets confirms
        self.remote_api_confirms = 'http://%s:%i%s' % (self.data_manager.conns_params['webServices']['marketConfirm']['host'],
                                                       self.data_manager.conns_params['webServices']['marketConfirm']['port'],
                                                       self.data_manager.conns_params['webServices']['marketConfirm']['service'])

    def define_market(self, vtn_id):
        """
        Define a market

        :param vtn_id: identifier of the VTN
        :type vtn_id: int
        :return: market parameters | FALSE if creation attempts were always not successful
        :rtype: dict | bool
        """
        for i in range(0, self.defaults['maxCreationAttempts']):
            # set the starting timestamp
            self.start_dt = Utils.get_start_dt(market_type=self.market_type)
            if self.start_dt is False:
                return False

            # get the maximum with the persistence model
            if self.data_manager.conns_params['forecastSource'] == 'InfluxDB':
                forecast_maxs = self.data_manager.influxdb_forecast(vtn_id=vtn_id, params={'type': self.market_type,
                                                                                           'start_dt': self.start_dt})
            else:
                forecast_maxs = self.data_manager.remote_forecast(vtn_id=vtn_id, params={'type': self.market_type,
                                                                                         'start_dt': self.start_dt})

            if forecast_maxs is not None:
                # define the token to stake
                ngt_to_stake = (forecast_maxs['maxUpper']-forecast_maxs['maxLower'])*self.defaults['revenueFactor']

                # define the settings
                settings = copy.deepcopy(self.defaults)
                settings.update(forecast_maxs)
                settings.update({'dsoStaking': ngt_to_stake, 'playerStaking': ngt_to_stake})

                # check if the market is confirmed
                if self.request_confirm(settings) is True:
                    return settings
                else:
                    # wait for a second before trying with a new definition
                    self.logger.warning('Market not confirmed, try again after 2 second waiting')
                    time.sleep(2)
            else:
                return False
        return False

    def request_confirm(self, settings):
        """
        Request a confirm for a market

        :param settings: market settings to confirm
        :type settings: dict
        :return: if confirmed TRUE, otherwise FALSE
        :rtype: bool
        """
        # perform the request
        r = Utils.perform_request(req_type=self.data_manager.conns_params['webServices']['marketConfirm']['request'],
                                  req_url=self.remote_api_confirms, req_params=settings, logger=self.logger)

        # check if the request has been successful
        if r is not None and r.status_code == 200:
            data = json.loads(r.text)
            return data['confirmed']
        else:
            return False
