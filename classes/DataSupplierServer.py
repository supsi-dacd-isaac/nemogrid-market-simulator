# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #

import json
import sys
import ssl

from urllib.parse import urlparse
from http.server import BaseHTTPRequestHandler, HTTPServer
from influxdb import InfluxDBClient

from classes.MarketsManager import MarketsManager

# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #

class DataSupplierServerHandler(BaseHTTPRequestHandler):
    """ DataSupplierServer handler """

    def do_GET(self):
        """ Handle a GET request """
        # parse the URL
        parsed_url = urlparse(self.path)

        self.server.supplier.logger.info('Service=\'%s\';Client=\'%s:%i\'' % (parsed_url.path, self.client_address[0],
                                                                              self.client_address[1]))

        # get the input parameters
        params = self._create_params_dict(input_array=parsed_url.query.split('&'))

        # check the key
        if self._check_key(input_params=params) is True:

            # retrieve a market manager address
            if parsed_url.path == '/MarketsManagerAddress':
                try:
                    # retrieve data
                    data = self.server.supplier.get_msm_address(params=params)

                    # send response
                    self._send_response(status_code=200, data=data)
                except Exception as e:
                    self.server.supplier.logger.error('Exception: %s' % str(e))
                    self._send_response(status_code=500, data={'desc': 'Unable to perform the request'})

            # retrieve the market info
            elif parsed_url.path == '/MarketInfo':
                try:
                    # retrieve data
                    [markets_manager, idx] = self.server.supplier.get_msm(params=params)

                    if idx is not None:
                        # gets the starting timestamp
                        state = markets_manager.get_state(idx=idx)

                        # check if the market is runnning
                        if state != 0:
                            data = {
                                        'idx': hex(idx),
                                        'state': state,
                                        'player': markets_manager.get_player(idx=idx),
                                        'startTS': markets_manager.get_start_time(idx=idx),
                                        'endTS': markets_manager.get_end_time(idx=idx),
                                        'dsoStaking': markets_manager.get_dso_stake(idx=idx),
                                        'playerStaking': markets_manager.get_player_stake(idx=idx),
                                        'upperThreshold': markets_manager.get_upper_maximum(idx=idx),
                                        'lowerThreshold': markets_manager.get_lower_maximum(idx=idx),
                                        'flag': markets_manager.get_flag(idx=idx),
                                        'maximumDSO': markets_manager.get_dso_maximum(idx=idx),
                                        'maximumPlayer': markets_manager.get_player_maximum(idx=idx),
                                        'tknsReleasedToDSO': markets_manager.get_tokens_released_to_dso(idx=idx),
                                        'tknsReleasedToPlayer': markets_manager.get_tokens_released_to_player(idx=idx),
                                   }
                        else:
                            data = {
                                        'idx': hex(idx),
                                        'state': state,
                                   }
                        # send the response
                        self._send_response(status_code=200, data=data)
                        self.server.supplier.logger.info('ResponseStatusCode=%i' % 200)
                    else:
                        self._send_response(status_code=500, data={'desc': 'Check your input parameters'})
                        self.server.supplier.logger.error('ResponseStatusCode=%i' % 500)

                except Exception as e:
                    self.server.supplier.logger.info.error('Exception: %s' % str(e))
                    self._send_response(status_code=500, data={'desc': 'Unable to perform the request'})
                    self.server.supplier.logger.error('ResponseStatusCode=%i' % 500)

            elif parsed_url.path == '/GetHistoricalData':
                try:
                    if self._check_last_param(last=params['last']) is True:
                        # retrieve data from InfluxDB server
                        query = 'SELECT %s(value) AS value FROM %s WHERE VTN=\'%s\' AND ' \
                                'time >= now() - %s GROUP BY time(%s)' % (params['function'],
                                                                          self.server.supplier.cfg['influxDB']['measurementData'],
                                                                          params['vtn'], params['last'],
                                                                          params['time_grouping'])
                        res = self.server.idb_client.query(query=query)
                        data = {'values': res.raw['series'][0]['values']}

                        # send response
                        self._send_response(status_code=200, data=data)
                    else:
                        str_err = 'Parameter \'last\' = %s not available (max 7d)' % params['last']
                        self.server.supplier.logger.error(str_err)
                        self._send_response(status_code=400, data={'desc': str_err})

                except Exception as e:
                    self.server.supplier.logger.error('Exception: %s' % str(e))
                    self._send_response(status_code=400, data={'desc': 'Unable to perform the request'})

            else:
                err_msg = {'desc':  'Service %s not available' % parsed_url.path}
                self.server.supplier.logger.error('Service \'%s\' not available' % parsed_url.path)
                self._send_response(status_code=500, data=err_msg)
                self.server.supplier.logger.error('ResponseStatusCode=%i' % 500)

        else:
            self._invalid_key_handling(params)

    def do_POST(self):
        """ Handle a GET request """
        # parse the URL
        parsed_url = urlparse(self.path)

        self.server.supplier.logger.info(
            'Service=\'%s\';Client=\'%s:%i\'' % (parsed_url.path, self.client_address[0], self.client_address[1]))

        # get the input parameters
        params = self._create_params_dict(input_array=parsed_url.query.split('&'))

        # check the key
        if self._check_key(input_params=params) is True:

            # retrieve a market manager address
            if parsed_url.path == '/PostForecast':
                try:
                    # build the InfluxDB point
                    point = {
                                'time': int(params['tstart']),
                                'measurement': self.server.supplier.cfg['influxDB']['measurementForecasts'],
                                'fields': dict(value=float(params['forecast'])),
                                'tags': dict(vtn=params['vtn'], type=params['type'])
                            }

                    # insert forecast data in InfluxDB
                    res = self.server.idb_client.write_points([point], time_precision='s')

                    # send response
                    if res is True:
                        self._send_response(status_code=200, data={'desc': 'Forecast successfully inserted'})
                    else:
                        self._send_response(status_code=500, data={'desc': 'Forecast not inserted'})

                except Exception as e:
                    self.server.supplier.logger.error('Exception: %s' % str(e))
                    self._send_response(status_code=500, data={'desc': 'Unable to perform the request'})

            else:
                err_msg = {'desc': 'Service %s not available' % parsed_url.path}
                self.server.supplier.logger.error('Service \'%s\' not available' % parsed_url.path)
                self._send_response(status_code=500, data=err_msg)
                self.server.supplier.logger.error('ResponseStatusCode=%i' % 500)

        else:
            self._invalid_key_handling(params)

    def _invalid_key_handling(self, params):
        if 'key' not in params.keys():
            err_msg = {'desc': 'Key not existing'}
            self.server.supplier.logger.error('Key not existing')
        else:
            err_msg = {'desc': 'Key \'%s\' not enabled' % params['key']}
            self.server.supplier.logger.error('Key %s not enabled' % params['key'])

        self._send_response(status_code=401, data=err_msg)
        self.server.supplier.logger.error('ResponseStatusCode=%i' % 401)

    def _send_response(self, status_code, data):
        self.send_response(status_code)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(data), 'utf8'))

    @staticmethod
    def _check_last_param(last):
        """ Check the last parameter for InfluxDb queries
        :param last: last string (ex: 7d)
        :type last: string
        :return True | False
        :rtype bool
        """
        # Not more than 7 days
        if last[-1] == 'd' and int(last[0:-1]) > 7:
            return False
        else:
            return True

    @staticmethod
    def _create_params_dict(input_array):
        """ Create a dictionary given an array of elements with format k=v (e.g. ['a=2.72', 'b=3.14', c='goofy']
        :param input_array: array of inputs
        :type input_array: list
        :return dictionary with the data
        :rtype dict
        """
        params = dict()
        for param in input_array:
            if '=' in param:
                (k, v) = param.split('=')
                params[k] = v
        return params

    def _check_key(self, input_params):
        """ Check the security key
        :param input_params: input parameters
        :type input_params: dict
        :return True if key enabled, otherwise False
        :rtype bool
        """
        if 'key' in input_params.keys() and input_params['key'] in self.server.supplier.cfg['server']['enabledKeys']:
            return True
        else:
            return False

class DataSupplierServer:

    def __init__(self, cfg, contracts, web3, logger):
        """ Constructor
        :param cfg: Configuration parameters
        :type: dict
        :param contracts: contracts dictionary
        :type: contracts object
        :param logger: logger
        :type: logger object
        """
        try:
            server = HTTPServer((cfg['server']['host'], int(cfg['server']['port'])), DataSupplierServerHandler)

            # enable the SSL usage
            if cfg['server']['protocol'] == 'HTTPS':
                server.socket = ssl.wrap_socket(server.socket, keyfile=cfg['server']['certificates']['keyFile'],
                                                certfile=cfg['server']['certificates']['certFile'], server_side=True)
                logger.info('Server running on https://%s:%s' % (cfg['server']['host'], cfg['server']['port']))
            else:
                logger.info('Server running on http://%s:%s' % (cfg['server']['host'], cfg['server']['port']))

            # set the DataSupplier instance
            server.supplier = DataSupplier(cfg, contracts, web3, logger)

            # set the InfluxDB client instance
            try:
                server.idb_client = InfluxDBClient(host=cfg['influxDB']['host'],
                                                   port=int(cfg['influxDB']['port']),
                                                   username=cfg['influxDB']['user'],
                                                   password=cfg['influxDB']['password'],
                                                   database=cfg['influxDB']['db'])
            except Exception as e:
                logger.error('EXCEPTION: %s' % str(e))
                sys.exit(2)
            logger.info('Connection successful')

            # launch the server
            server.serve_forever()

        except Exception as e:
            logger.error('Unable to launch the server: %s' % str(e))
            sys.exit()


class DataSupplier:

    def __init__(self, cfg, contracts, web3, logger=None):
        """ Constructor
        :param cfg: Configuration parameters
        :type cfg: dict
        :param contracts: dictionary containing the contracts instances
        :type contracts: dict
        :param web3: web3 provider to interact with the blockchain nodes
        :type web3: web3 object
        :param logger: logger
        :type: logger object
        """
        self.logger = logger
        self.contracts = contracts
        self.web3 = web3
        self.cfg = cfg

    def get_msm(self, params):
        """ Get a markets manager instance and its identifier
        :param params: input parameters
        :type params: dict
        :return MarketsManager instance, its identifier
        :rtype MarketsManager, int
        """
        try:
            addr_markets_manager = self.contracts['gsm'].get_address(self.web3.toChecksumAddress(params['dso']))
            sc_folder = self.cfg['smartContracts']['truffleProjectFolder']
            truffle_file = '%s/%s' % (sc_folder, self.cfg['smartContracts']['MarketsManager']['fileName'])

            # define the markets manager instance
            markets_manager = MarketsManager(web3=self.web3, address=self.web3.toChecksumAddress(addr_markets_manager),
                                             logger=self.logger,
                                             truffle_output_file=truffle_file, gas_price_factor=self.cfg['gasPriceFactor'])

            # get idx
            idx = markets_manager.calc_idx(address=self.web3.toChecksumAddress(params['player']),
                                           ts=int(params['ts_start']),market_type=int(params['type']))
            return markets_manager, idx
        except Exception as e:
            self.logger.error('Unable to create the MarketsManager instance with the passed parameters')
            return None, None

    def get_msm_address(self, params):
        """ Get the address of a markets manager
        :param params: input parameters
        :type params: dict
        :return dictionary containing the result
        :rtype dict
        """
        return {'address': self.contracts['gsm'].get_address(self.web3.toChecksumAddress(params['dso']))}
