# constants used in the project

# markets types
DAILY_MARKETS = 1
HOURLY_MARKETS = 2

# markets states
MARKETS_STATES = {
                    0: 'NONE',
                    1: 'NOT_RUNNING',
                    2: 'WAITING_CONFIRM_TO_START',
                    3: 'RUNNING',
                    4: 'WAITING_CONFIRM_TO_END',
                    5: 'WAITING_FOR_THE_REFEREE',
                    6: 'CLOSED',
                    7: 'CLOSED_AFTER_JUDGEMENT',
                    8: 'CLOSED_NOT_PLAYED',
                 }

# VTN ID
VTN_ID = 1
