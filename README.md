# Simulator for NEMoGrid Energy markets

The main aim of `simulator.py` script is to interact with the [smart contracts](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts) 
of [NEMoGrid](http://nemogrid.eu/) project in order to simulate energy markets. Besides, the script can be used to send manual commands to the contracts.

Here is reported a generic usage of `simulator.py`:
<pre>
python3 simulator.py -o CMD,ARG1,ARG2 -c conf/example.json 
</pre>

In the following `CMD` and `ARG` will be described.

# Python requirements

The script works with Python3.6 or above, it is recommended to create a virtual environment. 
The required modules can be installed with the following command:

<pre>
(venv) # pip install -r requirements.txt
</pre>

# Ethereum requirements
`simulator.py` interacts with an [Ethereum](https://www.ethereum.org/) blockchain where the smart contracts are 
deployed using a Web3 provider, which is also a synced node of the blockchain. 
Presently, `http` and `ipc` provider types are supported. 

Currently, instances of [`GroupsManager`](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts/blob/master/contracts/GroupsManager.sol), 
[`NGT`](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts/blob/master/contracts/NGT.sol) and [`DevicesList`](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts/blob/master/contracts/DevicesList.sol) smart contracts are deployed on the [Rinkeby](https://rinkeby.etherscan.io/) network testnet at the following addresses:
  
`GroupsManager`: [0xc89a7846353cef060501d66312bc53a6588e56ea](https://rinkeby.etherscan.io/address/0xc89a7846353cef060501d66312bc53a6588e56ea) 

`NGT`: [0xd2f651e8820b1ada1a776f0897282db9586ad854](https://rinkeby.etherscan.io/address/0xd2f651e8820b1ada1a776f0897282db9586ad854)

`DevicesList`: [0xf8675b439a78c6cfc91d7a54be28a7e72af0b7e3](https://rinkeby.etherscan.io/address/0xf8675b439a78c6cfc91d7a54be28a7e72af0b7e3)

If you want to play the markets using the aforementioned deployments you must:

1) Read and **understand** how the [NEMoGrid smart contracts](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts) work
2) Have a node synced to [Rinkeby](https://rinkeby.etherscan.io/) network. I did all the tests with [geth](https://github.com/ethereum/go-ethereum) client
3) Have at least four wallets (following named `owned`, `dso`, `player` and `referee`) on the node with a reasonable amount of ethers to perform the transactions
4) Request to [@dstreppa]('https://gitlab.com/dstreppa') a [`MarketsManager`](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts/blob/master/contracts/MarketsManager.sol) deployment and the related NGTs minting

# Start the simulator
Once the [`MarketsManager`](https://gitlab.com/supsi-dacd-isaac/nemogrid-smart-contracts/blob/master/contracts/MarketsManager.sol) 
contract has been deployed for your DSO on the address `address_markets_manager` you have to:

1) Properly set the token allowances:
<pre>
(venv) # python simulator.py -o ALLOW,dso,address_markets_manager,X -c conf/rinkeby.json
(venv) # python simulator.py -o ALLOW,player,address_markets_manager,X -c conf/rinkeby.json
</pre>  

With the commands above the allowances are set to X NGTs.

2) Be sure `data_retriever.py` is running and saving data in a proper InfluxDB database via REST requests (see configuration file `conf/conns_example.json` for details). The stored datasets will be used by `simulator.py`, (see Point 3). 
Regarding the requests format, please refer to the related [wiki](https://gitlab.com/supsi-dacd-isaac/nemogrid-market-simulator/wikis/Requests-format-and-specifications).

3) Simulate hourly market using the following crontab:

<pre>
50 * * * * cd $project_folder && venv/bin/python simulator.py -c conf/rinkeby.json -o OPEN_NEXT
10 * * * * cd $project_folder && venv/bin/python simulator.py -c conf/rinkeby.json -o SETTLE_LAST
</pre>  

At minute X:50, `simulator.py` opens a hourly market for the hour X+1, taking into account the settings in `conf/rinkeby.json`.  
At minute X:10, `simulator.py` settles the hourly market related to hour X-1, taking into account the settings in `conf/rinkeby.json`.
 
Considering `conf/rinkeby.json` file, `{"walletsAccount": {"dso"}} = 1` means that `eth.accounts[1]` of your `geth` client will be the `dso` in the markets.
Currently, no `referee` transactions are needed to properly settle the markets, i.e. `dso` and `player` never cheat. 

# Acknowledgements
The NEMoGrid project has received funding in the framework of the joint programming initiative ERA-Net Smart Energy Systems’ focus initiative Smart Grids Plus, with support from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 646039.
The authors would like to thank the Swiss Federal Office of Energy (SFOE) and the Swiss Competence Center for Energy Research - Future Swiss Electrical Infrastructure (SCCER-FURIES), for their financial and technical support to this research work.
