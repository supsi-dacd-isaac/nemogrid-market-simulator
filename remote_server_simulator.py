
# --------------------------------------------------------------------------- #
# Import section
# --------------------------------------------------------------------------- #

import argparse
import json
import random
import logging
import sys
import datetime
import calendar
import numpy as np

from urllib.parse import urlparse
from http.server import BaseHTTPRequestHandler, HTTPServer

# --------------------------------------------------------------------------- #
# Classes
# --------------------------------------------------------------------------- #
class Handler(BaseHTTPRequestHandler):
    """ Handler """

    def do_GET(self):
        self._handle_request()

    def do_POST(self):
        self._handle_request()

    def _handle_request(self):
        # parse the URL
        parsed_url = urlparse(self.path)

        # create data related to fake measurements
        if parsed_url.path == '/measurements':
            try:
                # create the fake data
                data = self._create_fake_measurement_data(params=parsed_url.query.split('&'))

                # send the response
                self._send_response(status_code=200, data=data)
            except Exception as e:
                err_msg = {'desc': str(e)}

                logging.error('Exception: %s' % str(e))
                self._send_response(status_code=500, data=err_msg)

        # create data related to fake confirms
        elif parsed_url.path == '/confirm':
            try:
                # create the fake confirm
                data = {'confirmed': True}

                # send the response
                self._send_response(status_code=200, data=data)
            except Exception as e:
                err_msg = {'desc': str(e)}

                logging.error('Exception: %s' % str(e))
                self._send_response(status_code=500, data=err_msg)


        # create data related to fake forecasts
        elif parsed_url.path == '/forecasts':
            try:
                # create the fake forecast
                data = self._create_fake_forecast_data(params=parsed_url.query.split('&'))

                # send the response
                self._send_response(status_code=200, data=data)
            except Exception as e:
                err_msg = {'desc': str(e)}

                logging.error('Exception: %s' % str(e))
                self._send_response(status_code=500, data=err_msg)
        else:
            err_msg = {'desc':  'Service %s not available' % parsed_url.path}

            logging.error('Service %s not available' % parsed_url.path)
            self._send_response(status_code=500, data=err_msg)

    def _send_response(self, status_code, data):
        self.send_response(status_code)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(data), 'utf8'))

    @staticmethod
    def _create_fake_measurement_data(params):

        vtn_id = None
        for param in params:
            (key, value) = param.split('=')
            if key == 'vtn':
                vtn_id = value

        # check if a VTN identifier has been found, if not raise an exception
        if vtn_id is None:
            raise ValueError('Unable to retrieve a VTN id from the request parameters')

        # create the data dictionary containing an array of data for the related VTN
        data = {
                    vtn_id: [
                                {
                                    'ts': int(calendar.timegm(datetime.datetime.utcnow().timetuple())) - 10,
                                    'power': round(random.uniform(min_power, max_power), 2)},
                                {
                                    'ts': int(calendar.timegm(datetime.datetime.utcnow().timetuple())) - 5,
                                    'power': round(random.uniform(min_power, max_power), 2)
                                }
                            ]
                }
        return data

    @staticmethod
    def _create_fake_forecast_data(params):
        vals = []
        vtn_id = ''
        for param in params:
            (ts, val) = param.split('=')
            if ts == 'vtn':
                vtn_id = val
            else:
                vals.append(float(val))

        return {vtn_id: np.mean(np.array(vals))}

# Main
if __name__ == "__main__":
    # get input arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-s', help='socket -> host:port. Example 192.168.1.22:8808')
    arg_parser.add_argument('-p', help='powers -> min;max. Example -p 5.3;35.8')
    arg_parser.add_argument('-l', help='log file')
    args = arg_parser.parse_args()

    # set logging object
    if not args.l:
        log_file = None
    else:
        log_file = args.l

    # logger
    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)-15s::%(levelname)s::%(funcName)s::%(message)s', level=logging.INFO,
                        filename=args.l)

    # set socket parameters
    try:
        (host, port) = args.s.split(':')
    except Exception as e:
        logger.error('Unable to get socket parameters from input argument \'%s\'' % args.s)
        sys.exit(-1)

    # set power minimum and maximum
    try:
        (min_power, max_power) = args.p.split(':')
        min_power = float(min_power)
        max_power = float(max_power)

        if max_power <= min_power:
            logger.error('Maximum (%.2f) smaller than minimum (%.2f)' % (max_power, min_power))
            sys.exit(-1)
    except Exception as e:
        logger.error('Unable to get powers min/max from input argument \'%s\'' % args.p)
        sys.exit(-2)

    logger.info('Starting server on socket %s:%s...' % (host, port))

    try:
        server = HTTPServer((host, int(port)), Handler)
        logger.info('Server successfully started')
        server.serve_forever()
    except Exception as e:
        logger.error('Unable to launch the server: %s' % str(e))
        sys.exit()



